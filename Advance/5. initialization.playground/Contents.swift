import UIKit

struct Fahrenheit {
    var temperature: Double
    init() {
        temperature = 32.0
    }
}
var f = Fahrenheit()
print("The default temperature is \(f.temperature)° Fahrenheit")

struct Celsius {
    var temperatureInCelsius: Double
    init(fromFahrenheit fahrenheit: Double) {
        temperatureInCelsius = (fahrenheit - 32.0) / 1.8
    }
    init(fromKelvin kelvin: Double) {
        temperatureInCelsius = kelvin - 273.15
    }
}
let boilingPointOfWater = Celsius(fromFahrenheit: 212.0)
let freezingPointOfWater = Celsius(fromKelvin: 273.15)
print("Water boils at \(boilingPointOfWater.temperatureInCelsius)° Celsius")

// Memberwise initializer
struct Size {
    var width = 0.0, height = 0.0
}
let twoByTwo = Size(width: 2.0, height: 2.0)

//class Size {
//    var width = 0.0, height = 0.0
//    init(width: Double, height: Double) {
//        self.width = width
//        self.height = height
//    }
//}
//let twoByTwo = Size(width: 2.0, height: 2.0)

// Default initializers
class ShoppingListItem {
    var name: String?
    var quantity = 1
    var purchased = false
}
var item = ShoppingListItem()

// initialzing subclasses
class Vehicle {
    var numberOfWheels = 0
    var description: String {
        return "\(numberOfWheels) wheel(s)"
    }
}

let vehicle = Vehicle()
print("Vehicle: \(vehicle.description)")

class Bicycle: Vehicle{
    override init() {
        super.init()
        numberOfWheels = 2
    }
//    var color = "blue"
}
let bicycle = Bicycle()
print("Bicycle: \(bicycle.description)")

// Designated initializers & Convenience initializers
class Food {
    var name: String
    init(name: String) {
        self.name = name
    }
    convenience init() {
        self.init(name: "[Unnamed]")
    }
}

class RecipeIngredient: Food {
    var quantity: Int
    init(name: String, quantity: Int) {
        self.quantity = quantity
        super.init(name: name)
    }
    override convenience init(name: String) {
        self.init(name: name, quantity: 1)
    }
//  override convenience init(){
    convenience init(){
        self.init(name: "test name", quantity: 1)
    }
    
}

struct Animal {
    let species: String
    init?(species: String) {
        if species.isEmpty { return nil }
        self.species = species
    }
}

let someCreature = Animal(species: "Giraffe")
if let giraffe = someCreature {
    print("An animal was initialized with a species of \(giraffe.species)")
}

let anonymousCreature = Animal(species: "")
if anonymousCreature == nil {
    print("The anonymous creature couldn't be initialized")
}

// required
class SomeClass {
    required init() {
        // initializer implementation goes here
    }
}

class SomeSubclass: SomeClass {
    // init() {
    required init() {
        // subclass implementation of the required initializer goes here
    }
}

// initialize property value using a closure
struct Chessboard {
    let boardColors: [Bool] = {
        var temporaryBoard = [Bool]()
        var isBlack = false
        for i in 1...8 {
            for j in 1...8 {
                temporaryBoard.append(isBlack)
                isBlack = !isBlack
            }
            isBlack = !isBlack
        }
        return temporaryBoard
    }()
    func squareIsBlackAt(row: Int, column: Int) -> Bool {
        return boardColors[(row * 8) + column]
    }
}

print("boardColors: \(Chessboard().boardColors)")

// Deinitialization
class Player {
    deinit {
    print("the class is going to be deallocated")
    }
}

var player: Player? = Player()
player = nil

// throw errors
enum VendingMachineError: Error {
    case invalidSelection
    case insufficientFunds(coinsNeeded: Int)
    case outOfStock
}

//throw VendingMachineError.insufficientFunds(coinsNeeded: 5)
func throw_error_function() throws {
    throw VendingMachineError.insufficientFunds(coinsNeeded: 5)
}

do{
    try throw_error_function()
    print("No error occured")
} catch VendingMachineError.insufficientFunds(let coinsNeeded) where coinsNeeded < 2{
    print("Cacthed error, the coinNeeded is smaller than 2")
} catch{
    print("Unexpected error: \(error) ")
}

let x = try? throw_error_function()

if let y = x {
    print("No error occured")
} else {
    print("the value of x is: \(x)")
}
