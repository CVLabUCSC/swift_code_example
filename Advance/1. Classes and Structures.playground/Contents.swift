import UIKit

struct Resolution {
    var width = 0
    var height = 0
}
class VideoMode {
    var resolution = Resolution()
    var interlaced = false
    var frameRate = 0.0
    var name: String?
}
let someResolution = Resolution()
let someVideoMode = VideoMode()
//someResolution.width = 1280
someVideoMode.frameRate = 30.0
print("someResolution width: \(someResolution.width)")
print("The width of someVideoMode is \(someVideoMode.resolution.width)")

// structures are value type
let hd = Resolution(width: 1920, height: 1080)
var cinema = hd
cinema.width = 2048
print("cinema is now \(cinema.width) pixels wide")
print("hd is still \(hd.width) pixels wide")

// classes are reference type
let tenEighty = VideoMode()
let alsoTenEighty = tenEighty
alsoTenEighty.frameRate = 30.0
print("The frameRate property of tenEighty is now \(tenEighty.frameRate)")

var classes_areIdentical = alsoTenEighty === tenEighty
