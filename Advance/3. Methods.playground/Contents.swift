import UIKit

struct Point {
    var x = 0.0, y = 0.0
    func isToTheRightOf(x: Double) -> Bool {
        return self.x > x
    }
    
    mutating func change_x_value(){
        self.x = 2.0
        print("The value of x has been changed to \(self.x)")
    }
}
var somePoint = Point(x: 4.0, y: 5.0)
//let somePoint = Point(x: 4.0, y: 5.0)
if somePoint.isToTheRightOf(x: 1.0) {
    print("This point is to the right of the line where x == 1.0")
}
somePoint.change_x_value()

struct example{
    static var test = 1
    static func test_function() {
        example.test += 1
        print("This is a static function: \(example.test)")
    }
}
example.test_function()

let instance_1 = example()
//instance_1.test_function()

// Subscripts
struct TimesTable {
    let multiplier: Int
    subscript(index: Int) -> Int {
        return multiplier * index
    }
}
let threeTimesTable = TimesTable(multiplier: 3)
print("six times three is \(threeTimesTable[6])")

struct MultTable{
    var test_value = 1
    subscript(rowInd: Int, colInd: Int) -> Int{
        return rowInd*colInd
    }
    
    subscript(rowInd: Int) -> Int{
        get{
            return rowInd
        }
        set{
            self.test_value = rowInd + newValue
            print("The sum of \(rowInd) and \(newValue) is: \(self.test_value)")
        }
    }
}
var myMulTable = MultTable()
let intN1 = 2, intN2 = 3
print("\(intN1) by \(intN2) is \(myMulTable[intN1, intN2])")

print(myMulTable[intN1])
myMulTable[intN1] = 3

