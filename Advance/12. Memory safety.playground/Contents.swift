import UIKit
// Conflicting access to In-Out parameters
func balance(_ x: inout Int, _ y: inout Int) {
    let sum = x + y
    x = sum / 2
    y = sum - x
}
var palyerOneScore = 42
//balance(&palyerOneScore, &palyerOneScore)

// Conflicting access to self in methods
struct Player {
    var health: Int
    mutating func shareHealth(with teammate: inout Player) {
            balance(&teammate.health, &health)
        }
}

var john = Player(health: 10)
//john.shareHealth(with: &john)

// coflicting access to properties
//balance(&john.health, &john.health)

// private access level
class aClassWithPrivate{
    private var aProp: Int = 0
}
var aS = aClassWithPrivate()
print(aS.aProp)
