import UIKit

//func swapTwoValues<random_type>(_ a: inout random_type, _ b: inout random_type) {
func swapTwoValues<T>(_ a: inout T, _ b: inout T) {
    let temporaryA = a
    a = b
    b = temporaryA
}

var someInt = 3
var anotherInt = 107
swapTwoValues(&someInt, &anotherInt)
print("someInt is now \(someInt), and anotherInt is now \(anotherInt)")

var someString = "hello"
var anotherString = "world"
swapTwoValues(&someString, &anotherString)
print("someString is now \(someString), and anotherString is now \(anotherString)")

// stack element
struct IntStack {
    var items = [Int]()
    mutating func push(_ item: Int) {
        items.append(item)
    }
    mutating func pop() -> Int {
        return items.removeLast()
    }
}

// You can view the values in stack using the button in right side
var stackOfInt = IntStack()
stackOfInt.push(1)
stackOfInt.push(2)
let lastIn = stackOfInt.pop()

struct Stack<Element> {
    var items = [Element]()
    mutating func push(_ item: Element) {
        items.append(item)
    }
    mutating func pop() -> Element {
        return items.removeLast()
    }
}

var stackOfStr = Stack<String>()
stackOfStr.push("one")
stackOfStr.push("two")
let lastIn_str = stackOfStr.pop()
