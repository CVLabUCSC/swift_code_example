import UIKit

extension Double {
    var km: Double { return self * 1_000.0 }
    var m: Double { return self }
    var cm: Double { return self / 100.0 }
    var mm: Double { return self / 1_000.0 }
    var ft: Double { return self / 3.28084 }
}
let oneInch = 25.4.mm
print("One inch is \(oneInch) meters")
// Prints "One inch is 0.0254 meters"
let threeFeet = 3.ft
print("Three feet is \(threeFeet) meters")

// you can also add new methods
extension Int {
    func repetitions(task: () -> Void) {
        for _ in 0..<self {
            task()
        }
    }
}

3.repetitions {
    print("Hello!")
}

// define a protocol
protocol FullyNamed {
    var fullName: String { get }
}

class Starship: FullyNamed {
    var prefix: String?
    var name: String
    init(name: String, prefix: String? = nil) {
        self.name = name
        self.prefix = prefix
    }
    var fullName: String {
        return (prefix != nil ? prefix! + " " : "") + name
    }
}
var ncc1701 = Starship(name: "Enterprise", prefix: "USS")
print("The full name of the starship is \(ncc1701.fullName)")

// Use protocol as types
class print_full_name {
    let instance: FullyNamed
    init(instance: FullyNamed) {
        self.instance = instance
        print("full name is: \(self.instance.fullName)")
    }
}

var full_name_printer = print_full_name(instance: ncc1701)

// delegate
protocol delegate_example {
    func example()
}

class class_A{
    let delegate: delegate_example
    init(delegate: delegate_example) {
        self.delegate = delegate
        self.delegate.example()
    }
}

class class_B: delegate_example {
    func example() {
        print("You are running the example function")
    }
}

class_A(delegate: class_B())
