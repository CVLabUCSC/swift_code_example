import UIKit

//final class Vehicle {
class Vehicle {
    var currentSpeed = 0.0
//    final var description: String {
    var description: String {
        return "traveling at \(currentSpeed) miles per hour"
    }
    func makeNoise() {
        // do nothing - an arbitrary vehicle doesn't necessarily make a noise
    }
}

let someVehicle = Vehicle()
print("Vehicle: \(someVehicle.description)")

class Bicycle: Vehicle {
    var hasBasket = false
}

let bicycle = Bicycle()
bicycle.hasBasket = true
bicycle.currentSpeed = 15.0
print("Bicycle: \(bicycle.description)")

class Car: Vehicle {
    override func makeNoise() {
        print("Vroom")
    }
}
let car = Car()
car.makeNoise()
//print(car.hasBasket)

class HonkingCar: Car{
    override func makeNoise() {
        super.makeNoise()
        print("Honk")
    }
}
let honkCar = HonkingCar()
honkCar.makeNoise()

// override properties
class Another_car: Vehicle {
    var gear = 1
    override var description: String {
        return super.description + " in gear \(gear)"
    }
}
let another_car = Another_car()
another_car.currentSpeed = 25.0
another_car.gear = 3
print("Car: \(another_car.description)")
