import UIKit

var someInts = [Int]()
print("someInts is of type [Int] with \(someInts.count) items.")
someInts.append(3)
someInts = []

var threeDoubles = Array(repeating: 0.0, count: 3)
var anotherThreeDoubles = Array(repeating: 2.5, count: 3)
var sixDoubles = threeDoubles + anotherThreeDoubles

//var shoppingList: [String] = ["Eggs", "Milk"]
var shoppingList = ["Eggs", "Milk"]
shoppingList.append("Flour")

shoppingList += ["cup"]
//shoppingList = shoppingList + ["cup"]
//shoppingList += "cup"

print("First element of the shoppingList: \(shoppingList[0])")

print("The length of shoppingList: \(shoppingList.count)")

// This would throw an error, since there is no fifth element in the array
//shoppingList[shoppingList.count] = "Salt"
//print(shoppingList)

// Change a range of values at once
shoppingList[1...] = ["Banana"]
print(shoppingList)

// insert an element
shoppingList.insert("Vanilla", at: 0)
print("Array after insert operation: \(shoppingList)")

// remove an element
let item = shoppingList.remove(at: 1)
print("Array after remove operation: \(shoppingList)")

// Iterate over an array
for item in shoppingList {
    print("Item: \(item)")
}

for (index, value) in shoppingList.enumerated(){
    print("index: \(index), value: \(value)")
}

// Set
var letters = Set<Character>()
letters.insert("a")
letters.insert("a")
print(letters.count)

// Initialize a set using an array
var favoriteGenres: Set<String> = ["Rock", "Classical", "Hip hop"]
//var favoriteGenres: Set = ["Rock", "Classical", "Hip hop"]

favoriteGenres.insert("Jazz")

if let removedGenre = favoriteGenres.remove("Rock") {
    print("\(removedGenre)? I'm over it.")
} else {
    print("I never much cared for that.")
}

if favoriteGenres.contains("Funk") {
    print("I get up on the good foot.")
} else {
    print("It's too funky in here.")
}

// iterate over set
for genre in favoriteGenres{
    print("\(genre)")
}

print("The sorted set:")
for genre in favoriteGenres.sorted(){
    print("\(genre)")
}

// Dictionary
var namesOfIntegers = [Int: String]()

// The type of the key must conform to the Hashable protocol
// let wrong_key_dict = Dictionary <(x: Int, y: Int), String>()

namesOfIntegers[16] = "sixteen"
print(namesOfIntegers)
namesOfIntegers = [:]

// dictionary initialization
var airports: [String: String] = ["YYZ": "Toronto Pearson", "DUB": "Dublin"]
airports["LHR"] = "London"
print("New dictionary: \(airports)")
// to change the element value
airports["LHR"] = "London Heathrow"
print("Changed dictionary: \(airports)")

// If you want to remove a key...
//airports["DUB"] = nil
//print(airports)

// Check if a key exist in the dictionary
if let airportName = airports["DUB"] {
    print("The name of the airport is \(airportName).")
} else {
    print("That airport isn't in the airports dictionary.")
}

// iterate
for (airportCode, airportName) in airports {
    print("\(airportCode): \(airportName)")
}

for airportCode in airports.keys {
    print("dictionary keys: \(airportCode)")
}

for airportName in airports.values {
    print("dictionary values: \(airportName)")
}

let airportCodes = [String](airports.keys)
let airportNames = [String](airports.values)

// value binding
let vegetable = "red pepper"
switch vegetable {
case "celery":
    print("Add some raisins and make ants on a log")
case "cucumber", "watercress":
    print("That would make a good tea sandwich")
case let x where x.hasSuffix("pepper"):
    print("Is it a spicy \(x)?")
default:
    print("Everything tastes good in soup.")
}
