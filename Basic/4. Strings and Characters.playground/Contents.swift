import UIKit

// String initialization
var emptyString = ""               // empty string literal
var anotherEmptyString = String()  // initializer syntax

// Strings are value types
var original_string = "hello"
var modified_string = original_string

modified_string += " world"
print("original string: \(original_string), modified string: \(modified_string)")

// Character
let exclamationMark: Character = "!"
let catCharacters: [Character] = ["C", "a", "t", "!", "🐱"]
let catString = String(catCharacters)
print(catString)

// Concatenate two strings
let string1 = "hello"
let string2 = " there"
var welcome = string1 + string2

// append a Character
welcome.append(exclamationMark)

// String interpolation
let multiplier = 3
let message = "\(multiplier) times 2.5 is \(Double(multiplier) * 2.5)"

// Count the number of characters in a string
var word = "cafe"
print("the number of characters in \(word) is \(word.count)")

// Substrings
let greeting = "Hello, world!"
let index = greeting.firstIndex(of: ",") ?? greeting.endIndex

let beginning = greeting[..<index]
print("type of beginning: \(type(of: beginning))")
// It's recommand that you convert the results to string
let newString = String(beginning)
print("type of newString: \(type(of: newString))")

// String comparison
let Str1 = "UCSC is number 1!", Str2 = "UCSC is the best!"
let areEqual = Str1 == Str2
let containsUCSC = Str1.hasPrefix("UCSC")
let contains_best = Str2.hasSuffix("the best!")


