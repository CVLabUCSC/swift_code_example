import UIKit

// Closed range
for index in 1...5 {
    print("Closed range: \(index)")
}

// Half-open range
for index in 1..<5 {
    print("Half-open range: \(index)")
}

// One-sided range
let numbers = [0, 1, 2, 3, 4, 5]
//print(numbers[2...])
//print(numbers[...2])
//print(numbers[..<2])

for index in 1... {
    print("Iterating forever...: \(index)")
}
