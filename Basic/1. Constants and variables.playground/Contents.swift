import UIKit

let maximumNumberOfLoginAttempts = 10
var currentLoginAttempt = 0
var x = 0.0, y = 0.0, z = 0.0

//maximumNumberOfLoginAttempts = 20
//currentLoginAttempt = 1.0
//x = 5.0

let cat = "🐱"; print(cat)

// This is a comment
// Type annotations
var welcomeMessage: String

//welcomeMessage = 1.0
welcomeMessage = "hello"

let pi = 3.14
let intergerPi = Int(pi)

let orangesAreOrange = true
if orangesAreOrange{
    print("Orange!")
}

// Tuples
let http404Error = (404, "Not Found")
// http404Error is of type (Int, String), and equals (404, "Not Found")
print(http404Error)

let (statusCode, statusMessage) = http404Error
print("The status code is \(statusCode)")
print("The status message is \(statusMessage)")

//let (statusCode, _) = http404Error
//print("The second status code is \(statusCode)")

// Notice that how we print the value of the variable here
print("The status code is \(http404Error.0), the status message is \(http404Error.1)")

let http200Status = (statusCode: 200, description: "OK")
print("The status code is \(http200Status.statusCode), the status message is \(http200Status.description)")
