import UIKit

// Function
func sayHelloWorld() -> String {
    return "hello, world"
}
print(sayHelloWorld())

func greet(person: String) -> String{
    let greeting = "Hello, " + person
    return greeting
}
print(greet(person: "Roberto"))

func greet(person: String, alreadyGreeted: Bool) -> String{
    if alreadyGreeted {
        return greet(person: person) + " again!"
    } else {
        return greet(person: "Roberto")
    }
}
print(greet(person: "Roberto", alreadyGreeted: false))

//// Function with parameters and labels
//func greet(person p: String, alreadyGreeted a: Bool) -> String{
//    if a {
//        return greet(person: p) + " again!"
//    } else {
//        return greet(person: p)
//    }
//}
//print(greet(person: "Roberto", alreadyGreeted: true))

// You can also use function without labels
func greet(_ person: String) -> String{
    let greeting = "Hello, " + person
    return greeting
}
print(greet("Roberto"))

// You don't really need to return a value
func printSum(_ num1: Double, _ num2: Double){
    print(num1 + num2)
}

printSum(1, 2)

// You can assign default value to the input parameter
func nRoot(_ aNumber: Double, rootOrder: Double = 2) -> Double {
    return pow(aNumber, 1/rootOrder)
}
print(nRoot(4))
print(nRoot(8, rootOrder: 3))

// Variadic parameters
func arithmeticMean(_ numbers: Double...) -> Double {
    var total: Double = 0
    for number in numbers {
        total += number
    }
    return total / Double(numbers.count)
}
print(arithmeticMean(1, 2, 3, 4, 5))
print(arithmeticMean(8, 8.25))

// multiple return values & optional return values
func minMax(array: [Int]) -> (min: Int, max: Int)? {
    if array.isEmpty { return nil }
    var currentMin = array[0]
    var currentMax = array[0]
    for value in array[1..<array.count] {
        if value < currentMin {
            currentMin = value
        } else if value > currentMax {
            currentMax = value
        }
    }
    return (currentMin, currentMax)
}

if let bounds = minMax(array: [8, -6, 2, 109, 3, 71]) {
    print("min is \(bounds.min) and max is \(bounds.max)")
}

// Change parameters in function
func swapTwoInts(_ a: inout Int, _ b: inout Int) {
    let temporaryA = a
    a = b
    b = temporaryA
}
var someInt = 3
var anotherInt = 107
swapTwoInts(&someInt, &anotherInt)
print("someInt is now \(someInt), and anotherInt is now \(anotherInt)")

// memory conflict
var stepSize = 1
func incrementInPlace(_ number: inout Int){
    number += stepSize
}
//incrementInPlace(&stepSize)
var copyOfStepSize = stepSize
incrementInPlace(&copyOfStepSize)
stepSize = copyOfStepSize

// Optional
func simpleFunc1(x: Int?) {
    if x == nil || x!<=0{
        print("Unacceptable input value")
        return
    }
    print("Input value is \(x!)")
}
simpleFunc1(x: 3)
simpleFunc1(x: nil)
simpleFunc1(x: -3)

let possibleNumber = "5"
//let possibleNumber = "abc"
// Optional binding
if let actualNumber = Int(possibleNumber) {
    print("possibleNumber has an interger value of : \(actualNumber)")
} else {
    print("possibleNumber could not be converted to an integer")
}

func simpleFunc2(x: Int?){
    if let y = x, y > 0{
        print("Input value is \(y)")
    } else {
        print("Unaccepted input value")
    }
}
simpleFunc2(x: Int(possibleNumber))

assert(true, "this condition is not met")

func simpleFunc3(x: Int?){
    guard let y=x, y>0 else {
        print("Unacceptabel input value")
        return
    }
    print("Input value is \(y)")
}

simpleFunc3(x: Int(possibleNumber))

// Function type
func addTwoInts(_ a: Int, _ b: Int) -> Int {
    return a + b
}
func multiplyTwoInts(_ a: Int, _ b: Int) -> Int {
    return a * b
}

var mathFunction: (Int, Int) -> Int = addTwoInts
print("Result: \(mathFunction(2, 3))")
mathFunction = multiplyTwoInts
print("Result: \(mathFunction(2, 3))")

func printMathResult(_ mathFunction: (Int, Int) -> Int, _ a: Int, _ b: Int) {
    print("Result: \(mathFunction(a, b))")
}
printMathResult(addTwoInts, 3, 5)

func chooseIntMathFunc(isSum: Bool) -> (Int, Int) -> Int {
    return isSum ? addTwoInts : multiplyTwoInts
}

let num1 = 3, num2 = 5
var theIntMathFunc = chooseIntMathFunc(isSum: true)

print("Type of theIntMathFunc: \(type(of: theIntMathFunc))")

print(theIntMathFunc(num1, num2))
theIntMathFunc = chooseIntMathFunc(isSum: false)
print(theIntMathFunc(num1, num2))
