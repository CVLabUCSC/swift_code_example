import UIKit

var convertedNumber: Int?
//convertedNumber = Int("abc")
convertedNumber = Int("123")

if convertedNumber == nil{
    print("Bad conversion")
} else {
//    convertedNumber = nil
    print(convertedNumber!)
}

var nickName: String?
var fullName: String = "Roberto"

// nickName = "Peng"
let informalGreeting = "Hi \(nickName ?? fullName)"

// ternary conditional operator
let return_first_name = true
let Name = return_first_name ? "Roberto" : "Peng"
