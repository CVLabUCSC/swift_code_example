import UIKit

func chooseIntMathFunc(isSum: Bool) -> (Int, Int) -> Int {
    let inside_constant = 1
    func addTwoInts(_ a: Int, _ b: Int) -> Int {
        return a + b + inside_constant
    }
    func multiplyTwoInts(_ a: Int, _ b: Int) -> Int {
        return a * b
    }
    return isSum ? addTwoInts : multiplyTwoInts
}

let num1 = 3, num2 = 5
//print(addTwoInts(num1, num2))
print(chooseIntMathFunc(isSum: true)(num1, num2))

// closure
// normal function
let names = ["Chris", "Alex", "Ewa", "Barry", "Daniela"]
func backward(_ s1: String, _ s2: String) -> Bool {
    return s1 > s2
}
var reversedNames = names.sorted(by: backward)
reversedNames = names.sorted(by: { (s1: String, s2: String) -> Bool in
    return s1 > s2
})
reversedNames = names.sorted(by: { s1, s2 in return s1 > s2 } )
reversedNames = names.sorted(by: { s1, s2 in s1 > s2 } )
reversedNames = names.sorted(by: { $0 > $1 } )
reversedNames = names.sorted(by: >)

// trailing closure
reversedNames = names.sorted(){$0 > $1}
reversedNames = names.sorted{$0 > $1}
print(reversedNames)
// map function
let numbers = [1, 2, 3]
let results = numbers.map{
    (x: Int) -> Int in return x + 1
}

// Capture value
func makeIncrementer(forIncrement amount: Int) -> () -> Int {
    var runningTotal = 0
    func incrementer() -> Int {
        runningTotal += amount
        return runningTotal
    }
    return incrementer
}

let incrementByTen = makeIncrementer(forIncrement: 10)
incrementByTen()
incrementByTen()
incrementByTen()

let incrementBySeven = makeIncrementer(forIncrement: 7)
incrementBySeven()
incrementBySeven()
incrementBySeven()

incrementByTen()

let alsoIncrementByTen = incrementByTen
alsoIncrementByTen()

enum CompassPoint {
    case north
    case south
    case east
    case west
}

var directionToHead: CompassPoint
directionToHead = .north

directionToHead = .south
switch directionToHead {
case .north:
    print("Lots of planets have a north")
case .south:
    print("Watch out for penguins")
case .east:
    print("Where the sun rises")
case .west:
    print("Where the skies are blue")
}

// raw values
enum CompassPoint_int: Int {
    case north
    case south
    case east
    case west
}
print(CompassPoint_int.east.rawValue)

// initialize using the rawValue
let new_direction = CompassPoint_int(rawValue: 0)

enum CompassPoint_string: String {
    case north
    case south
    case east
    case west
}
print(CompassPoint_string.east.rawValue)
